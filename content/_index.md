
Mitch O'Farrell terrorizes the houseless, colludes with cops and uses his city trust fund to bribe developers. Let's get him out of City Hall.


## Tweet at Mitch

Tell Mitch to resign with [a simple tweet](https://ctt.ac/M_Nhp)

## Sign the petition

Sign Ground Game's [petition to get Mitch to resign](https://actionnetwork.org/petitions/resign-mitch-ofarrell/)

## Send Mitch an email

Use our toolkit to [email Mitch a message](https://bit.ly/emailmof)


## Read more about why Mitch needs to go
- [Torturing houseless people](https://knock-la.com/echo-park-lake-march-24-sweep-fence-mitch-ofarrell/)
- [Refusing to meet with Elysian Valley about pedestrian deaths](https://www.lataco.com/three-dead-24-hours-westlake-north-hollywood-torrance/)
- [Discriminating against street vendors in Hollywood](https://www.lataco.com/new-special-enforcement-zone-against-bulky-items-is-fresh-attempt-to-keep-bacon-wrapped-hot-dog-vendors-out-of-hollywood/)
- [Bribing developers](https://michaelkohlhaas.org/wp/2019/01/15/a-detailed-analysis-of-the-cash-flowing-in-and-out-of-mitch-ofarrells-public-benefits-slush-fund-developers-pay-hundreds-of-thousands-of-dollars-for-the-privilege-of-building-out-of-code-p/)
- [Dodging the public records act](https://michaelkohlhaas.org/wp/2020/05/13/mitch-ofarrell-and-the-california-public-records-act-his-office-violates-it-so-egregiously-that-comparisons-fail-me-heres-the-story-of-one-request-from-may/)
- [Ktown for All's statement](https://drive.google.com/file/d/1OoQwV0o6roWjqWaIW6Fkgg_QK2OGnge4/view)
- [People's City Council's statement](https://www.peoplescitycouncil-la.com/press-releases/echo-park-rise-up)
